package com.example.finalkotlin_tagne_ulrich

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class MyListAdapter(private val context: Activity,private  val list:ArrayList<Parking>)
    :ArrayAdapter<Parking>(context,R.layout.custom_list,list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.custom_list,null,true)

        val numerotext = rowView.findViewById(R.id.numero) as TextView
        val statustext = rowView.findViewById(R.id.status) as TextView
        val immatriculation = rowView.findViewById(R.id.immatriculation) as TextView

        numerotext.text = list.get(position).numero.toString()
        immatriculation.text = list.get(position).immatriculation
        if (list.get(position).est_occuper ==true){

            statustext.text = "Occupé"

        }else {
            statustext.text = "Libre"
        }

        return rowView
    }
}