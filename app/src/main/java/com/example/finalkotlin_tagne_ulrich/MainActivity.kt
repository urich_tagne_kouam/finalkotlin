package com.example.finalkotlin_tagne_ulrich

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ListView

class MainActivity : AppCompatActivity() {

    val list = ArrayList<Parking>()
    val listDispo = ArrayList<Parking>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //la liste
        list.add(Parking(1,true,"123456"))
        list.add(Parking(2,false,""))
        list.add(Parking(3,true,"123456"))
        list.add(Parking(4,false,""))
        list.add(Parking(5,false,""))
        list.add(Parking(6,true,"123456"))
        list.add(Parking(7,true,"123456"))
        list.add(Parking(8,false,""))
        list.add(Parking(9,false,""))
        list.add(Parking(10,false,""))
        list.add(Parking(11,true,"123456"))
        list.add(Parking(12,true,"123456"))
        list.add(Parking(13,false,""))
        list.add(Parking(14,true,"123456"))
        list.add(Parking(15,true,"123456"))
        list.add(Parking(16,true,"123456"))
        list.add(Parking(17,false,""))
        list.add(Parking(18,false,""))

        val maliste = findViewById<ListView>(R.id.ma_liste)

        val myListAdapter = MyListAdapter(this,list)
        maliste.adapter = myListAdapter

        maliste.setOnItemClickListener(){adapterView,view,position,id->
            val itemAtPos = adapterView.getItemAtPosition(position)
            val itemIDAtPos = adapterView.getItemAtPosition(position)



        }



    }

    fun dispo(view: View) {

        for(i in list)
        {
            if(i.est_occuper == false){
                listDispo.add(i)
            }
        }
        val maliste = findViewById<ListView>(R.id.ma_liste)
        val myListAdapter = MyListAdapter(this,listDispo)
        maliste.adapter = myListAdapter

    }

    fun toute(view: View) {


        val maliste = findViewById<ListView>(R.id.ma_liste)
        val myListAdapter = MyListAdapter(this,list)
        maliste.adapter = myListAdapter

    }
}